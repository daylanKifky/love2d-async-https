local https = require "https"

url, options, channel = ...
code, body, headers = https.request(url, options)
channel:push( {code, body, headers} )
