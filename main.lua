package.cpath="./?.so/?.dll"


local timer  -- A timer used to animate our circle.
local modulePath = select(1, ...):match("(.-)[^%.]+$")

---------------------------------------------------
local https_handler = {counter = 0}

function https_handler:request(url, options, _handler)
    if self.thread and self.thread:isRunning( ) then
        return false
    end

    self.channel = love.thread.newChannel( )
    self.thread = love.thread.newThread( (modulePath:gsub("%.", "/").."thread_func.lua") )
    self.thread:start(url, options, self.channel)

    if _handler then
        self.handler = _handler
    else 
        self.handler = function(r)
            self.counter = 0 
            code, body, headers = unpack(r)
            print("HTTPS_HANDLER recv:", code )
        end
    end

    return true
end

function https_handler:receive(dt)
    if not self.channel then return false end
    self.counter  = self.counter + dt

    local response = self.channel:peek()
    if response then
        self.channel:pop()
        if self.handler then self.handler(response) end
        return true, unpack(response)
    end
    return false
end

function https_handler:get_error()
    if self.thread then return self.thread:getError() 
    else return nil end
end

return https_handler
